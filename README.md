# Spring AOP Demo

Sample Spring Boot project demonstrating non-trivial usage of Aspect-oriented Programming with AspectJ.

## Project structure

* `RemoteController` - Simulation of remote service with two endpoints
  * returns success with configurable delay
  * returns success every 4th call, first 3 attempts return code 500, it uses **global** counter
* `AppController` - Controller methods (requesting remote service) that are used in Aspects
* `IntegrationTest` - For running `AppController` endpoints in automated, repeatable way, with some assertions (but look at logs to see how it works exactly)

## Aspects

Three conflicting advices can be turned on/off by placing annotation on method: `@ApplyMetrics`, `@ApplyCache`, `@ApplyRetry` (this one additionally has configurable number of retries).

Every Advice is in a separate Aspect class, because strict execution order was implemented: `Metrics` -> `Cache` -> `Retry`
* `Retry` is wrapped with `Cache`, it's cached only when success was returned after retries
* All are wrapped by `Metrics`, it counts total execution time of cached and non-cached requests

## Run & observe

Run tests and see logs.

`runSlowUrl`
* Executes twice endpoint with `Cache` and `Metrics` Advices
* First execution is not cached, second returns response from cache

`runFailingUrlWithNineRetriesAndExpectSuccess`
* Executes twice failing service with 9 retries (remember that 4th call is successful)
* First execution is successful, second returns response from cache

`runFailingUrlWithOneRetryAndExpectFailure`
* Executes once failing service with 1 retry
* Finishes with unhandled exception, `Cache` and `Metrics` Advices are not completed (e.g. no information of execution time in logs)
