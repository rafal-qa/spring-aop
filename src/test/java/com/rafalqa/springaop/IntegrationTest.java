package com.rafalqa.springaop;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@Slf4j
public class IntegrationTest {

  @Autowired private WebTestClient testClient;

  @Test
  void runSlowUrl() {
    for (int i = 1; i <= 2; i++) {
      logRunNumber(i);

      testClient
          .get()
          .uri("/slow/100")
          .exchange()
          .expectStatus()
          .isOk()
          .expectBody(String.class)
          .isEqualTo("[Remote service] OK after 100 ms");
    }
  }

  @Test
  void runFailingUrlWithNineRetriesAndExpectSuccess() {
    for (int i = 1; i <= 2; i++) {
      logRunNumber(i);

      testClient
          .get()
          .uri("/retry-failing-9")
          .exchange()
          .expectStatus()
          .isOk()
          .expectBody(String.class)
          .isEqualTo("[Remote service] OK after count 4");
    }
  }

  @Test
  void runFailingUrlWithOneRetryAndExpectFailure() {
    testClient.get().uri("/retry-failing-1").exchange().expectStatus().is5xxServerError();
  }

  private void logRunNumber(int number) {
    log.info(String.format("===== %s =====", number));
  }
}
