package com.rafalqa.springaop.remote;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/remote-service")
@RequiredArgsConstructor
@Slf4j
public class RemoteController {

  private final Counter counter;

  @GetMapping("/slow/{timeout}")
  public String getSlow(@PathVariable Integer timeout) throws InterruptedException {
    log.info("GET /remote-service/slow/" + timeout);

    TimeUnit.MILLISECONDS.sleep(timeout);
    return "[Remote service] OK after " + timeout + " ms";
  }

  @GetMapping("/failing")
  public String getFailing() throws InterruptedException {
    log.info("GET /remote-service/failing");

    TimeUnit.MILLISECONDS.sleep(100);

    int nextVal = counter.getNext();

    if (nextVal >= 4) {
      counter.reset();
      return "[Remote service] OK after count " + nextVal;
    }

    throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
