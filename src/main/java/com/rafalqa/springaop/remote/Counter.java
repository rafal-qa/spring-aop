package com.rafalqa.springaop.remote;

import org.springframework.stereotype.Component;

@Component
public class Counter {
  private volatile int value = 0;

  public synchronized int getNext() {
    value++;
    return value;
  }

  public void reset() {
    value = 0;
  }
}
