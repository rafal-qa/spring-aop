package com.rafalqa.springaop;

import com.rafalqa.springaop.aspect.annotation.ApplyCache;
import com.rafalqa.springaop.aspect.annotation.ApplyMetrics;
import com.rafalqa.springaop.aspect.annotation.ApplyRetry;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

@RestController
@RequiredArgsConstructor
public class AppController {

  private final WebClient client = WebClient.create("http://localhost:8080");

  @ApplyCache
  @ApplyMetrics
  @GetMapping("/slow/{timeout}")
  public String getSlow(@PathVariable Integer timeout) {
    return client
        .get()
        .uri("/remote-service/slow/" + timeout)
        .retrieve()
        .toEntity(String.class)
        .block()
        .getBody();
  }

  @ApplyMetrics
  @ApplyCache
  @ApplyRetry(9)
  @GetMapping("/retry-failing-9")
  public String getFailingOK() {
    return client
        .get()
        .uri("/remote-service/failing")
        .retrieve()
        .toEntity(String.class)
        .block()
        .getBody();
  }

  @ApplyMetrics
  @ApplyCache
  @ApplyRetry(1)
  @GetMapping("/retry-failing-1")
  public String getFailingException() {
    return client
        .get()
        .uri("/remote-service/failing")
        .retrieve()
        .toEntity(String.class)
        .block()
        .getBody();
  }
}
