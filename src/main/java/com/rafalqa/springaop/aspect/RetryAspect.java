package com.rafalqa.springaop.aspect;

import com.rafalqa.springaop.aspect.annotation.ApplyRetry;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClientResponseException;

@Order(102)
@Aspect
@Component
@Slf4j
public class RetryAspect {

  @Around("@annotation(applyRetry)")
  public Object retryAdvice(ProceedingJoinPoint joinPoint, ApplyRetry applyRetry) throws Throwable {
    log.info("retryAdvice");

    int maxRetries = applyRetry.value();
    int attempt = 0;

    WebClientResponseException webClientResponseException;

    log.info("Max retries: " + maxRetries);

    do {
      attempt++;
      try {
        log.info("Attempt: " + attempt);
        return joinPoint.proceed();
      } catch (WebClientResponseException e) {
        webClientResponseException = e;
      }
    } while (attempt <= maxRetries);

    throw webClientResponseException;
  }
}
