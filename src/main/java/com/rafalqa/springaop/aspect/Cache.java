package com.rafalqa.springaop.aspect;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class Cache {
  public Map<String, String> store = new ConcurrentHashMap<>();
}
