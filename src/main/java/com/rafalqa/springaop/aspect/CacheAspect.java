package com.rafalqa.springaop.aspect;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Order(101)
@Aspect
@Component
@RequiredArgsConstructor
@Slf4j
public class CacheAspect {

  private final Cache cache;

  @Pointcut("@annotation(com.rafalqa.springaop.aspect.annotation.ApplyCache)")
  private void cache() {}

  @Around("cache()")
  public Object cacheAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
    log.info("cacheAdvice");

    String cacheKey =
        joinPoint.getSignature().toShortString() + "#" + Arrays.toString(joinPoint.getArgs());

    Object result;

    if (cache.store.containsKey(cacheKey)) {
      log.info("From cache: " + cacheKey);
      result = cache.store.get(cacheKey);
    } else {
      log.info("NOT in cache: " + cacheKey);
      result = joinPoint.proceed();
      cache.store.put(cacheKey, (String) result);
    }

    return result;
  }
}
