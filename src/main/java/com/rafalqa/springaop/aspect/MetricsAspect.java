package com.rafalqa.springaop.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;

@Order(100)
@Aspect
@Component
@Slf4j
public class MetricsAspect {

  @Pointcut("@annotation(com.rafalqa.springaop.aspect.annotation.ApplyMetrics)")
  private void metrics() {}

  @Around("metrics()")
  public Object metricsAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
    log.info("metricsAdvice");

    Instant startTime = Instant.now();
    Object result = joinPoint.proceed();
    Duration duration = Duration.between(startTime, Instant.now());

    log.info("Execution time: " + duration.toMillis() + " ms");

    return result;
  }
}
